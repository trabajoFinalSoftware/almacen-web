import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatRadioModule } from '@angular/material/radio';
import { MatTabsModule } from '@angular/material/tabs';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { NoFoundComponent } from './pages/no-found/no-found.component';
import { DashboardComponent } from './pages/home/dashboard/dashboard.component';
import { SalePageComponent } from './pages/home/sale-page/sale-page.component';
import { ProductsComponent } from './pages/home/products/products.component';
import { ReasonsComponent } from './pages/home/reasons/reasons.component';
import { ProvidersComponent } from './pages/home/providers/providers.component';
import { UsersComponent } from './pages/home/users/users.component';
import { HomeComponent } from './pages/home/home.component';
import { AppAbreviacionPipe } from './pipes/app-abreviacion/app-abreviacion.pipe';
import { ProductsEditComponent } from './pages/home/products/products-edit/products-edit.component';
import { ProvidersEditComponent } from './pages/home/providers/providers-edit/providers-edit.component';
import { ReasonsEditComponent } from './pages/home/reasons/reasons-edit/reasons-edit.component';
import { UsersEditComponent } from './pages/home/users/users-edit/users-edit.component';
import { ActionDialogComponent } from './components/action-dialog/action-dialog.component';
import { NoResultsTableComponent } from './components/no-results-table/no-results-table.component';
import { ToastService } from './services/toast/toast.service';
import { DatePipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NoFoundComponent,
    DashboardComponent,
    SalePageComponent,
    ProductsComponent,
    ReasonsComponent,
    ProvidersComponent,
    UsersComponent,
    HomeComponent,
    AppAbreviacionPipe,
    ProductsEditComponent,
    ProvidersEditComponent,
    ReasonsEditComponent,
    UsersEditComponent,
    ActionDialogComponent,
    NoResultsTableComponent,
    
  ],
  imports: [
    BrowserModule,
    MatIconModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatTableModule,
    MatInputModule,
    MatPaginatorModule,
    MatButtonModule,
    MatDialogModule,
    FormsModule,
    MatSnackBarModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatTabsModule,
  ],
  providers: [
    ToastService,
    { provide: LOCALE_ID, useValue: 'en-GB' },
    DatePipe,
    MatDatepickerModule,  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
