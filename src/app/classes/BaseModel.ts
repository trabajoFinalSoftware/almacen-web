export class BaseModel {
  id?: string | null = "";
  createDate?: Date | null = null;
  updateDate?: Date | null = null;
  active?: boolean = true;
}
