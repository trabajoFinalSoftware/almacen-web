import { HttpClient } from "@angular/common/http";
import { AppApiService, AppHttpErrorCallback as AppHttpErrorLlamada } from "../repository/app-api/app-api.service";
import { ActionLog } from "./ActionLog";
import { BaseModel } from "./BaseModel";
import { Order } from "./Order";
import { Product } from "./Product";
import { Reason } from "./Reason";
import { User } from "./User";

export type ProductLogsHttpCall = (productLogs: ProductLog[]) => void;
export type ProductLogHttpCall = (productLog: ProductLog) => void;

export class ProductLog extends BaseModel {
  productId?: string | null = "";
  product?: Product | null = null;
  userId?: string | null = "";
  user?: User | null = null;
  actionLogId?: string | null = "";
  actionLog?: ActionLog | null = null;
  reasonId?: string | null = "";
  reason?: Reason | null = null;
  orderId?: string | null = "";
  order?: Order | null = null;
  amount?: number | null = 0;
  isDeleteAction?: boolean | null = false;

  static getAllPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: ProductLogsHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.PRODUCTLOGS + filter;
    AppApiService.get(http, url, success, error);
  }

  static getAllInputPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: ProductLogsHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.PRODUCTLOGS+ "/input" + filter;
    AppApiService.get(http, url, success, error);
  }

  static getAllOutputPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: ProductLogsHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.PRODUCTLOGS + "/output" + filter;
    AppApiService.get(http, url, success, error);
  }

  static get(http: HttpClient, id: any, success: ProductLogHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.PRODUCTLOG + "/" + id;
    AppApiService.get(http, url, success, error);
  }

  static post(http: HttpClient, object: any, success: ProductLogHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.create(http, AppApiService.PRODUCTLOG, object, success, error);
  }

  static update(http: HttpClient, object: any, success: ProductLogHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.update(http, AppApiService.PRODUCTLOG, object, success, error);
  }

  static delete(http: HttpClient, id: any, success: ProductLogHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.PRODUCTLOG + "/delete/" + id;
    AppApiService.deleteLogic(http, url, success, error);
  }
}
