import { HttpClient } from "@angular/common/http";
import { AppApiService, AppHttpErrorCallback as AppHttpErrorLlamada } from "../repository/app-api/app-api.service";
import { BaseModel } from "./BaseModel";
import { PayType } from "./PayType";
import { User } from "./User";

export type OrdersHttpCall = (orders: Order[]) => void;
export type OrderHttpCall = (order: Order) => void;

export class Order extends BaseModel {
  date?: Date | null = null;
  userId?: string | null = "";
  user: User | null = null;
  payTypeId?: string | null = "";
  payType?: PayType | null = null;
  total?: number | null = 0;

  static getAllPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: OrdersHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.ORDERS + filter;
    AppApiService.get(http, url, success, error);
  }

  static get(http: HttpClient, id: any, success: OrderHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.ORDER + "/" + id;
    AppApiService.get(http, url, success, error);
  }

  static post(http: HttpClient, object: any, success: OrderHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.create(http, AppApiService.ORDER, object, success, error);
  }

  static update(http: HttpClient, object: any, success: OrderHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.update(http, AppApiService.ORDER, object, success, error);
  }

  static delete(http: HttpClient, id: any, success: OrderHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.ORDER + "/delete/" + id;
    AppApiService.deleteLogic(http, url, success, error);
  }
}
