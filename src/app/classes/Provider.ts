import { HttpClient } from "@angular/common/http";
import { AppApiService, AppHttpErrorCallback as AppHttpErrorLlamada } from "../repository/app-api/app-api.service";
import { BaseModel } from "./BaseModel";

export type ProvidersHttpCall = (providers: Provider[]) => void;
export type ProviderHttpCall = (provider: Provider) => void;

export class Provider extends BaseModel {
  name?: string | null = "";
  adress?: string | null = "";
  phone?: string | null = "";
  email?: string | null = "";

  static getAllPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: ProvidersHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.PROVIDERS + filter;
    AppApiService.get(http, url, success, error);
  }

  static get(http: HttpClient, id: any, success: ProviderHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.PROVIDER + "/" + id;
    AppApiService.get(http, url, success, error);
  }

  static post(http: HttpClient, object: any, success: ProviderHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.create(http, AppApiService.PROVIDER, object, success, error);
  }

  static update(http: HttpClient, object: any, success: ProviderHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.update(http, AppApiService.PROVIDER, object, success, error);
  }

  static delete(http: HttpClient, id: any, success: ProviderHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.PROVIDER + "/delete/" + id;
    AppApiService.deleteLogic(http, url, success, error);
  }
}
