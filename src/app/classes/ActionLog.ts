import { HttpClient } from "@angular/common/http";
import { AppApiService, AppHttpErrorCallback as AppHttpErrorLlamada } from "../repository/app-api/app-api.service";
import { BaseModel } from "./BaseModel";

export type ActionLogsHttpCall = (actionLogs: ActionLog[]) => void;
export type ActionLogHttpCall = (actionLog: ActionLog) => void;

export class ActionLog extends BaseModel {
  staticCode: number = 0;
  description: string = "";

  static getAllPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: ActionLogsHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.ACTIONLOGS + filter;
    AppApiService.get(http, url, success, error);
  }

  static get(http: HttpClient, id: any, success: ActionLogHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.ACTIONLOG + "/" + id;
    AppApiService.get(http, url, success, error);
  }

  static post(http: HttpClient, object: any, success: ActionLogHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.create(http, AppApiService.ACTIONLOG, object, success, error);
  }

  static update(http: HttpClient, object: any, success: ActionLogHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.update(http, AppApiService.ACTIONLOG, object, success, error);
  }

  static delete(http: HttpClient, id: any, success: ActionLogHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.ACTIONLOG + "/delete/" + id;
    AppApiService.deleteLogic(http, url, success, error);
  }
}
