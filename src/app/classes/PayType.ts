import { HttpClient } from "@angular/common/http";
import { AppApiService, AppHttpErrorCallback as AppHttpErrorLlamada } from "../repository/app-api/app-api.service";
import { BaseModel } from "./BaseModel";

export type PayTypesHttpCall = (payTypes: PayType[]) => void;
export type PayTypeHttpCall = (payType: PayType) => void;

export class PayType extends BaseModel {
  staticCode: number = 0;
  description: string = "";

  static getAllPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: PayTypesHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.PAYTYPES + filter;
    AppApiService.get(http, url, success, error);
  }

  static get(http: HttpClient, id: any, success: PayTypeHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.PAYTYPE + "/" + id;
    AppApiService.get(http, url, success, error);
  }

  static post(http: HttpClient, object: any, success: PayTypeHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.create(http, AppApiService.PAYTYPE, object, success, error);
  }

  static update(http: HttpClient, object: any, success: PayTypeHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.update(http, AppApiService.PAYTYPE, object, success, error);
  }

  static delete(http: HttpClient, id: any, success: PayTypeHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.PAYTYPE + "/delete/" + id;
    AppApiService.deleteLogic(http, url, success, error);
  }
}
