import { HttpClient } from "@angular/common/http";
import { AppApiService, AppHttpErrorCallback as AppHttpErrorLlamada } from "../repository/app-api/app-api.service";
import { BaseModel } from "./BaseModel";

export type ReasonsHttpCall = (reasons: Reason[]) => void;
export type ReasonHttpCall = (reason: Reason) => void;

export class Reason extends BaseModel {
  staticCode?: number = 0;
  description: string = "";
  actionLogId: string = "";

  static getAllPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: ReasonsHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.REASONS + filter;
    AppApiService.get(http, url, success, error);
  }

  static getAllOutPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: ReasonsHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.REASONS + "/out" + filter;
    AppApiService.get(http, url, success, error);
  }

  static get(http: HttpClient, id: any, success: ReasonHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.REASON + "/" + id;
    AppApiService.get(http, url, success, error);
  }

  static post(http: HttpClient, object: any, success: ReasonHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.create(http, AppApiService.REASON, object, success, error);
  }

  static update(http: HttpClient, object: any, success: ReasonHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.update(http, AppApiService.REASON, object, success, error);
  }

  static delete(http: HttpClient, id: any, success: ReasonHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.REASON + "/delete/" + id;
    AppApiService.deleteLogic(http, url, success, error);
  }
}
