import { HttPResponseModel } from "./HttpResponseModel";

export class HttpArrayResponse extends HttPResponseModel {
  Results: any = null;
  Total: Number = 0;
}
