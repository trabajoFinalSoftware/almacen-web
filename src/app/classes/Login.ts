import { HttpClient } from "@angular/common/http";
import { AppApiService, AppHttpErrorCallback as AppHttpErrorLlamada } from "../repository/app-api/app-api.service";
import { BaseModel } from "./BaseModel";
import { UserAuth } from "./UserAuth";

export type LoginHttpLlamada = (usuario: UserAuth) => void;

export class Login extends BaseModel {
  usuario: string = "";
  contrasena: string = "";

  static login(http: HttpClient, object: any, success: LoginHttpLlamada, error: AppHttpErrorLlamada) {
    AppApiService.create(http, AppApiService.LOGIN, object, success, error);
  }

}
