import { HttpClient } from "@angular/common/http";
import { AppApiService, AppHttpErrorCallback as AppHttpErrorLlamada } from "../repository/app-api/app-api.service";
import { BaseModel } from "./BaseModel";
import { Provider } from "./Provider";

export type ProductsHttpCall = (products: Product[]) => void;
export type ProductHttpCall = (product: Product) => void;

export class Product extends BaseModel {
  expiryDate?: Date | null = null;
  name?: string | null = "";
  providerId?: string | null = "";
  provider?: Provider | null = null;
  stock?: number | null = 0;
  price?: number | null = 0;
  amountLog?: number = 0;

  static getAllPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: ProductsHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.PRODUCTS + filter;
    AppApiService.get(http, url, success, error);
  }

  static getAllLowStockPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: ProductsHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.PRODUCTS + "/lowstock" + filter;
    AppApiService.get(http, url, success, error);
  }

  static get(http: HttpClient, id: any, success: ProductHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.PRODUCT + "/" + id;
    AppApiService.get(http, url, success, error);
  }

  static post(http: HttpClient, object: any, success: ProductHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.create(http, AppApiService.PRODUCT, object, success, error);
  }

  static postLog(http: HttpClient, object: any, success: ProductHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.PRODUCT + "/log";
    AppApiService.create(http, url, object, success, error);
  }

  static update(http: HttpClient, object: any, success: ProductHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.update(http, AppApiService.PRODUCT, object, success, error);
  }

  static updateLog(http: HttpClient, object: any, success: ProductHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.PRODUCT + "/log";
    AppApiService.update(http, url, object, success, error);
  }

  static delete(http: HttpClient, id: any, success: ProductHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.PRODUCT + "/delete/" + id;
    AppApiService.deleteLogic(http, url, success, error);
  }
}
