import { HttpClient } from "@angular/common/http";
import { AppApiService, AppHttpErrorCallback as AppHttpErrorLlamada } from "../repository/app-api/app-api.service";
import { BaseModel } from "./BaseModel";
import { Order } from "./Order";
import { Product } from "./Product";

export type ProductOrdersHttpCall = (productOrders: ProductOrder[]) => void;
export type ProductOrderHttpCall = (productOrder: ProductOrder) => void;

export class ProductOrder extends BaseModel {
  productId?: string | null = "";
  product?: Product | null = null;
  orderId?: string | null = "";
  order?: Order | null = null;
  amount?: number | null = 0;

  static getAllPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: ProductOrdersHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.PRODUCTORDERS + filter;
    AppApiService.get(http, url, success, error);
  }

  static get(http: HttpClient, id: any, success: ProductOrderHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.PRODUCTORDER + "/" + id;
    AppApiService.get(http, url, success, error);
  }

  static post(http: HttpClient, object: any, success: ProductOrderHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.create(http, AppApiService.PRODUCTORDER, object, success, error);
  }

  static update(http: HttpClient, object: any, success: ProductOrderHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.update(http, AppApiService.PRODUCTORDER, object, success, error);
  }

  static delete(http: HttpClient, id: any, success: ProductOrderHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.PRODUCTORDER + "/delete/" + id;
    AppApiService.deleteLogic(http, url, success, error);
  }
}
