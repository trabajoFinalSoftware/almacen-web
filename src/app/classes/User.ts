import { HttpClient } from "@angular/common/http";
import { AppApiService, AppHttpErrorCallback as AppHttpErrorLlamada } from "../repository/app-api/app-api.service";
import { BaseModel } from "./BaseModel";
import { Role } from "./Role";

export type UsersHttpCall = (users: User[]) => void;
export type UserHttpCall = (user: User) => void;

export class User extends BaseModel {
  firstName?: string | null = "";
  lastName?: string | null = "";
  nick: string | null = "";
  password?: string | null = "";
  role?: Role | null = null;
  roleId?: string | null = "";

  static getAllPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: UsersHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.USERS + filter;
    AppApiService.get(http, url, success, error);
  }

  static get(http: HttpClient, id: any, success: UserHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.USER + "/" + id;
    AppApiService.get(http, url, success, error);
  }

  static post(http: HttpClient, object: any, success: UserHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.create(http, AppApiService.USER, object, success, error);
  }

  static update(http: HttpClient, object: any, success: UserHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.update(http, AppApiService.USER, object, success, error);
  }

  static delete(http: HttpClient, id: any, success: UserHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.USER + "/delete/" + id;
    AppApiService.deleteLogic(http, url, success, error);
  }
}
