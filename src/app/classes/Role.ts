import { HttpClient } from "@angular/common/http";
import { AppApiService, AppHttpErrorCallback as AppHttpErrorLlamada } from "../repository/app-api/app-api.service";
import { BaseModel } from "./BaseModel";

export type RolesHttpCall = (roles: Role[]) => void;
export type RoleHttpCall = (role: Role) => void;

export class Role extends BaseModel {
  staticCode: number = 0;
  description: string = "";

  static getAllPag(http: HttpClient, index: number, elementsTotal: number, search: string, success: RolesHttpCall, error: AppHttpErrorLlamada) {
    let filter: string = "/" + (index + 1) + "/" + elementsTotal + ((search === '') ? '' : ('?search=' + search));
    let url = AppApiService.ROLES + filter;
    AppApiService.get(http, url, success, error);
  }

  static get(http: HttpClient, id: any, success: RoleHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.ROLE + "/" + id;
    AppApiService.get(http, url, success, error);
  }

  static post(http: HttpClient, object: any, success: RoleHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.create(http, AppApiService.ROLE, object, success, error);
  }

  static update(http: HttpClient, object: any, success: RoleHttpCall, error: AppHttpErrorLlamada) {
    AppApiService.update(http, AppApiService.ROLE, object, success, error);
  }

  static delete(http: HttpClient, id: any, success: RoleHttpCall, error: AppHttpErrorLlamada) {
    let url = AppApiService.ROLE + "/delete/" + id;
    AppApiService.deleteLogic(http, url, success, error);
  }
}
