import { HttPResponseModel } from "./HttpResponseModel";

export class HttpErrorResponse extends HttPResponseModel {
  Error: string = "";
}
