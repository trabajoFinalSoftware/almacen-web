import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { NestedTreeControl } from '@angular/cdk/tree';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

interface FoodNode {
  name: string;
  icon: string;
  url?: string;
  children?: FoodNode[];
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  treeControl = new NestedTreeControl<FoodNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<FoodNode>();
  hasChild = (_: number, node: FoodNode) => !!node.children && node.children.length > 0;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  user: any = null;

  constructor(private router: Router, private breakpointObserver: BreakpointObserver) { }

  ngOnInit(): void {
    if (localStorage.getItem("USER") == null) {
      this.router.navigate(['login']);
    } else {
      this.user = JSON.parse(localStorage.getItem("USER") as string);
    }

    this.mainMenuSetup();
  }

  logout() {
    localStorage.removeItem("USER")
    this.router.navigate(['login']);
  }

  private mainMenuSetup() {
    let homeMenu: FoodNode [] = [
      {
        name: 'Reportes',
        icon: 'home',
        url: '/'
      },
      {
        name: 'Entradas/Salidas',
        icon: 'shopping_cart',
        url: 'ventas'
      },
      {
        name: 'Productos',
        icon: 'store',
        url: 'productos'
      },
    ];

    let adminMenu = this.getAdminMenu();

    for (let item of adminMenu)
      homeMenu.push(item);

    this.dataSource.data = homeMenu;
  }

  private getAdminMenu(): FoodNode [] {
    return [
      {
        name: 'Usuarios',
        icon: 'manage_accounts',
        url: 'usuarios'
      },
      {
        name: 'Proveedores',
        icon: 'connect_without_contact',
        url: 'proveedores'
      },
      {
        name: 'Razones',
        icon: 'description',
        url: 'razones'
      },
    ];
  }
}
