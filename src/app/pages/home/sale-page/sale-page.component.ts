import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AppApiService } from 'src/app/repository/app-api/app-api.service';
import { ToastService } from 'src/app/services/toast/toast.service';
import { PayType } from '../../../classes/PayType';
import { Product } from '../../../classes/Product';
import { Reason } from '../../../classes/Reason';
import { User } from '../../../classes/User';
import { ActionDialogComponent } from '../../../components/action-dialog/action-dialog.component';


@Component({
  selector: 'app-sale-page',
  templateUrl: './sale-page.component.html',
  styleUrls: ['./sale-page.component.scss']
})
export class SalePageComponent implements OnInit {

  private timeOut: any;

  products: Product[] = [];
  reasons: Reason[] = [];
  payTypes: PayType[] = [];

  displayedColumns: string[] = ['index', 'name', 'provider', 'expiryDate', 'stock', 'price', 'action'];

  tableLength = 0;
  tableIndex = 0;
  pageLength = 10;
  search = "";

  operation: any = "1";
  reasonSelected: Reason = new Reason;
  user: User = new User;
  payTypeSelected: PayType = new PayType;

  dialogoRef?: MatDialogRef<ActionDialogComponent> | null = null;

  constructor(private http: HttpClient, private route: Router, private dialog: MatDialog, private toast: ToastService, private activedRoute: ActivatedRoute, private api: AppApiService) { }

  ngOnInit(): void {
    Reason.getAllOutPag(this.http, 0, 100, "", reasons => this.setReasons(reasons), error => this.showError(error));
    PayType.getAllPag(this.http, 0, 100, "", payTypes => this.setPayTypes(payTypes), error => this.showError(error));
    this.getProducts();
  }

  onPageChange(event: PageEvent) {
    this.tableIndex = event.pageIndex;
    this.pageLength = event.pageSize;
    let params: Params = {
      pageLength: event.pageSize,
      pageIndex: event.pageIndex,
    }
    this.getProducts();

    this.changeParams(params);
  }

  applyFilter(event: any) {
    if (this.timeOut) {
      clearTimeout(this.timeOut)
    }
    this.timeOut = setTimeout(() => {
      this.tableIndex = 0;
      let params: Params = {
        buscar: event,
        pageIndex: this.tableIndex,
      }
      this.changeParams(params);

      this.getProducts();
    }, 500);
  }

  changeParams(params: Params) {
    this.route.navigate([],
      {
        relativeTo: this.activedRoute,
        queryParams: params,
        replaceUrl: true,
        queryParamsHandling: 'merge',

      });

  }

  getProducts() {
    Product.getAllPag(this.http,
      this.tableIndex,
      this.pageLength,
      this.search,
      (products) => this.setProductList(products),
      (error) => this.showError(error));
  }

  setReasons(reasons: Reason[]) {
    this.reasons = reasons;
  }

  setPayTypes(payTypes: PayType[]) {
    this.payTypes = payTypes;
  }

  inputLogDialog(product: Product) {

    if (product.amountLog == null || product.amountLog == 0) {
      this.toast.showErrorMessage("Ingrese cantidad valida");
      return;
    }

    this.dialogoRef = this.dialog.open(ActionDialogComponent, {
      data: {
        title: 'Advertencia',
        message: `¿Estás seguro de registrar el re-stock de ${product.amountLog} unidades del producto ${product.name}?`,
        positiveButton: 'Aceptar', //Si dan click al boton positivo retorna true
        negativeButton: 'Cancelar' //Si dan click al boton Negatibo retorna false
      },
      width: '50%',
      minWidth: '280px',
      maxWidth: '560px'
    });
    this.dialogoRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.logRegister(product);
      }
    });
  }

  outputLogDialog(product: Product) {

    if (product.amountLog == null || product.amountLog == 0) {
      this.toast.showErrorMessage("Ingrese cantidad valida");
      return;
    }

    this.dialogoRef = this.dialog.open(ActionDialogComponent, {
      data: {
        title: 'Advertencia',
        message: `¿Estás seguro de registrar la salida de ${product.amountLog} unidades del producto ${product.name} por motivo de ${this.reasonSelected.description}?`,
        positiveButton: 'Aceptar', //Si dan click al boton positivo retorna true
        negativeButton: 'Cancelar' //Si dan click al boton Negatibo retorna false
      },
      width: '50%',
      minWidth: '280px',
      maxWidth: '560px'
    });
    this.dialogoRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.logRegister(product);
      }
    });
  }

  logRegister(product: Product) {

    this.user = JSON.parse(localStorage.getItem("USER") as string);

    var productLogFormat: any = {
      product: product,
      userId: this.user.id,
      actionLogId: this.operation,
      amount: product.amountLog,
      reasonId: this.reasonSelected.id,
    }

    Product.updateLog(this.http, productLogFormat, (provider) => this.showSuccess(), (error) => this.showError(error));
  }

  showSuccess() {
    if (this.operation == 1)
      this.inputMessage();
    else
      this.outputMessage();
  }

  private setProductList(productsList: Product[]) {
    this.products = productsList;
    this.toast.showSuccessMessage("Lista de productos actualizada.");
  }

  private inputMessage() {
    this.toast.showSuccessMessage("Entrada registrada con exito.");
    this.getProducts();
  }

  private outputMessage() {
    this.toast.showSuccessMessage("Salida registrada con exito.");
    this.getProducts();
  }

  private showError(error: string) {
    this.toast.showErrorMessage(error);
  }
}
