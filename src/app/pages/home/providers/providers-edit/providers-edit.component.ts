import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Provider } from '../../../../classes/Provider';
import { AppApiService } from '../../../../repository/app-api/app-api.service';
import { ToastService } from '../../../../services/toast/toast.service';

@Component({
  selector: 'app-providers-edit',
  templateUrl: './providers-edit.component.html',
  styleUrls: ['./providers-edit.component.scss']
})
export class ProvidersEditComponent implements OnInit {

  form: any;
  errorMessage = null;
  providerId = null;
  action: string = "";

  constructor(private http: HttpClient, private router: Router, private currentRoute: ActivatedRoute, public toast: ToastService, private formBuider: FormBuilder, private api: AppApiService) {
    this.currentRoute.params.subscribe(res => this.providerId = res['id']);
  }

  ngOnInit() {
    this.form = this.formBuider.group({
      name: ['', [Validators.required]],
      address: ['', [Validators.required]],
      phone: ['', [
        Validators.required,
        Validators.pattern("^[0-9]*$"),
        Validators.minLength(6),
      ]],
      email: ['', [Validators.required]],
    });

    if (this.providerId == 0) {
      this.action = "Crear";
    }
    else {
      this.action = "Editar";

      Provider.get(this.http, this.providerId, (provider) => this.getProvider(provider), (error) => this.showError(error));
    }
  }

  closeAlert(event: Event) {
    this.errorMessage = null;
  }

  createOrUpdate() {

    let obj = null;
    if (this.form)
      obj = this.form.value;
    this.errorMessage = null;

    var body: Provider = {
      name: obj.name,
      adress: obj.address,
      phone: obj.phone,
      email: obj.email,
    }

    if (this.providerId == 0) {
      Provider.post(this.http, body, (provider) => this.create(provider), (error) => this.showError(error))
    }
    else {
      body.id = this.providerId;
      Provider.update(this.http, body, (provider) => this.update(provider), (error) => this.showError(error))
    }
  }

  getProvider(provider: Provider) {
    this.form?.controls['name'].setValue(provider.name);
    this.form?.controls['address'].setValue(provider.adress);
    this.form?.controls['phone'].setValue(provider.phone);
    this.form?.controls['email'].setValue(provider.email);
  }

  create(provider: Provider) {
    this.toast.showSuccessMessage("Proveedor registrado con exito");
    this.return();
  }

  update(reason: Provider) {
    this.toast.showSuccessMessage("Proveedor actualizado con exito.");
    this.return();
  }

  showError(error: string) {
    this.toast.showErrorMessage(error);
  }

  return() {
    this.router.navigate(['../proveedores']);
  }
}
