import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AppApiService } from 'src/app/repository/app-api/app-api.service';
import { ToastService } from 'src/app/services/toast/toast.service';
import { Provider } from '../../../classes/Provider';
import { ActionDialogComponent } from '../../../components/action-dialog/action-dialog.component';

@Component({
  selector: 'app-providers',
  templateUrl: './providers.component.html',
  styleUrls: ['./providers.component.scss']
})
export class ProvidersComponent implements OnInit {

  private timeOut: any;

  providers: Provider[] = [];

  displayedColumns: string[] = ['index', 'name', 'address', 'phone', 'email', 'action'];

  tableLength = 0;
  tableIndex = 0;
  pageLength = 10;
  search = "";

  dialogoRef?: MatDialogRef<ActionDialogComponent> | null = null;

  constructor(private http: HttpClient, private route: Router, private dialog: MatDialog, private toast: ToastService, private activedRoute: ActivatedRoute, private api: AppApiService) {
    this.activedRoute.queryParams.subscribe(params => {
      this.pageLength = (params['pageLength'] != undefined) ? parseInt(params['pageLength']) : this.pageLength;
      this.tableIndex = (params['pageIndex'] != undefined) ? parseInt(params['pageIndex']) : this.tableIndex;
      this.search = (params['buscar'] != undefined) ? (params['buscar']) : this.search;
    }).unsubscribe();

  }

  ngOnInit(): void {
    this.getProviders();
  }

  onPageChange(event: PageEvent) {
    this.tableIndex = event.pageIndex;
    this.pageLength = event.pageSize;
    let params: Params = {
      pageLength: event.pageSize,
      pageIndex: event.pageIndex,
    }
    this.getProviders();

    this.changeParams(params);
  }

  edit(id: any) {
    this.route.navigate(['proveedores/edicion/' + id]);
  }

  getProviders() {
    Provider.getAllPag(this.http,
      this.tableIndex,
      this.pageLength,
      this.search,
      (providers) => this.setProviderList(providers),
      (error) => this.showError(error));
  }

  deleteDialog(id: bigint, description: string) {
    this.dialogoRef = this.dialog.open(ActionDialogComponent, {
      data: {
        title: 'Advertencia',
        message: `¿Estás seguro de eliminar el proveedor ${description}?`,
        positiveButton: 'Eliminar', //Si dan click al boton positivo retorna true
        negativeButton: 'Cancelar' //Si dan click al boton Negatibo retorna false
      },
      width: '50%',
      minWidth: '280px',
      maxWidth: '560px'
    });
    this.dialogoRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.providerDelete(id);
      }
    });
  }

  providerDelete(id: any) {
    Provider.delete(this.http, id,
      () => this.providerDeleteMessage(),
      (error) => this.showError(error));
  }

  changeParams(params: Params) {
    this.route.navigate([],
      {
        relativeTo: this.activedRoute,
        queryParams: params,
        replaceUrl: true,
        queryParamsHandling: 'merge',

      });

  }

  applyFilter(event: any) {
    if (this.timeOut) {
      clearTimeout(this.timeOut)
    }
    this.timeOut = setTimeout(() => {
      this.tableIndex = 0;
      let params: Params = {
        buscar: event,
        pageIndex: this.tableIndex,
      }
      this.changeParams(params);

      this.getProviders();
    }, 500);
  }

  private setProviderList(reasonsList: Provider[]) {
    this.providers = reasonsList;
  }

  private providerDeleteMessage() {
    this.toast.showSuccessMessage("Proveedor eliminado con exito.");
    this.getProviders();
  }

  private showError(error: string) {
    this.toast.showErrorMessage(error);
  }
}

