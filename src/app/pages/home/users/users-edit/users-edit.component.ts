import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Role } from '../../../../classes/Role';
import { User } from '../../../../classes/User';
import { AppApiService } from '../../../../repository/app-api/app-api.service';
import { ToastService } from '../../../../services/toast/toast.service';

@Component({
  selector: 'app-users-edit',
  templateUrl: './users-edit.component.html',
  styleUrls: ['./users-edit.component.scss']
})
export class UsersEditComponent implements OnInit {

  form: any;

  errorMessage = null;
  usuarioId = null;
  roles: Role[] = [];
  accion: string = "";
  esEdicion: boolean = false;

  constructor(private http: HttpClient, private router: Router, private currentRoute: ActivatedRoute, public toast: ToastService, private formBuider: FormBuilder, private api: AppApiService) {
    this.currentRoute.params.subscribe(res => this.usuarioId = res['id']);
  }

  ngOnInit() {
    this.form = this.formBuider.group({
      nombres: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      usuario: ['', [Validators.required]],
      contrasena: ['', this.esEdicion ? [Validators.required] : []],
      rol: ['', [Validators.required]],
    });

    Role.getAllPag(
      this.http,
      0,
      1000,
      "",
      (roles) => this.obtenerRoles(roles),
      (error) => this.mostrarError(error));

    if (this.usuarioId == 0) {
      this.accion = "Crear";
      this.esEdicion = false;
    }
    else {
      this.accion = "Editar";
      this.esEdicion = true;

      User.get(this.http, this.usuarioId, (usuario) => this.obtenerUsuario(usuario), (error) => this.mostrarError(error));
    }
  }

  closeAlert(event: Event) {
    this.errorMessage = null;
  }

  agregarUsuario() {
    let url = null;
    let obj = null;
    if (this.form)
      obj = this.form.value;
    this.errorMessage = null;

    var body: User = {
      firstName: obj.nombres,
      lastName: obj.apellidos,
      nick: obj.usuario,
      roleId: obj.rol,
    }

    if (this.usuarioId == 0) {
      body.password = obj.contrasena;
      User.post(this.http, body, (usuario) => this.crearUsuario(usuario), (error) => this.mostrarError(error))
    }
    else {
      body.id = this.usuarioId;
      User.update(this.http, body, (usuario) => this.actualizarUsuario(usuario), (error) => this.mostrarError(error))
    }
  }

  obtenerUsuario(usuario: User) {
    debugger
    this.form?.controls['nombres'].setValue(usuario.firstName);
    this.form?.controls['apellidos'].setValue(usuario.lastName);
    this.form?.controls['usuario'].setValue(usuario.nick);
    this.form?.controls['rol'].setValue(usuario.roleId);
  }

  crearUsuario(usuario: User) {
    this.toast.showSuccessMessage("Usuario creado con exito");
    this.retornar();
  }

  actualizarUsuario(usuario: User) {
    this.toast.showSuccessMessage("Usuario actualizoda con exito.");
    this.retornar();
  }

  obtenerRoles(roles: Role[]) {
    this.roles = roles;
  }

  mostrarError(error: string) {
    this.toast.showErrorMessage(error);
  }

  retornar() {
    this.router.navigate(['../usuarios']);
  }
}
