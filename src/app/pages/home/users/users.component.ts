import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AppApiService } from 'src/app/repository/app-api/app-api.service';
import { ToastService } from 'src/app/services/toast/toast.service';
import { User } from '../../../classes/User';
import { ActionDialogComponent } from '../../../components/action-dialog/action-dialog.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  private tiempoFueraId: any;

  users: User[] = [];

  displayedColumns: string[] = ['indice', 'usuario', 'nombre', 'rol', 'acciones'];

  tablaLongitud = 0;
  tablaIndicePagina = 0;
  tablaTamanoPagina = 10;
  indiceCompensacion = 0;
  buscar = "";

  dialogoRef?: MatDialogRef<ActionDialogComponent> | null = null;

  constructor(private http: HttpClient, private ruta: Router, private dialogo: MatDialog, private toast: ToastService, private rutaActual: ActivatedRoute, private api: AppApiService) {
    this.rutaActual.queryParams.subscribe(params => {
      this.tablaTamanoPagina = (params['tamanoPagina'] != undefined) ? parseInt(params['tamanoPagina']) : this.tablaTamanoPagina;
      this.tablaIndicePagina = (params['indicePagina'] != undefined) ? parseInt(params['indicePagina']) : this.tablaIndicePagina;
      this.buscar = (params['buscar'] != undefined) ? (params['buscar']) : this.buscar;
    }).unsubscribe();

  }

  ngOnInit(): void {
    this.getUsuariosPaginados();
  }

  onPageChange(event: PageEvent) {
    this.tablaIndicePagina = event.pageIndex;
    this.tablaTamanoPagina = event.pageSize;
    let parametros: Params = {
      tamanoPagina: event.pageSize,
      indicePagina: event.pageIndex,
    }
    this.getUsuariosPaginados();

    this.changeParams(parametros);
  }

  editarUsuario(id: any) {
    this.ruta.navigate(['usuarios/edicion/' + id]);
  }

  getUsuariosPaginados() {
    User.getAllPag(this.http,
      this.tablaIndicePagina,
      this.tablaTamanoPagina,
      this.buscar,
      (usuarios) => this.getUsuarios(usuarios),
      (error) => this.mostrarError(error));
  }

  dialogoEliminar(id: bigint, description: string) {
    this.dialogoRef = this.dialogo.open(ActionDialogComponent, {
      data: {
        title: 'Advertencia',
        message: `¿Estás seguro de eliminar el usuario ${description}?`,
        positiveButton: 'Eliminar', //Si dan click al boton positivo retorna true
        negativeButton: 'Cancelar' //Si dan click al boton Negatibo retorna false
      },
      width: '50%',
      minWidth: '280px',
      maxWidth: '560px'
    });
    this.dialogoRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.userDelete(id);
      }
    });
  }

  userDelete(id: any) {
    User.delete(this.http, id,
      () => this.eliminarUsuarioMensaje(),
      (error) => this.mostrarError(error));
  }

  changeParams(params: Params) {
    this.ruta.navigate([],
      {
        relativeTo: this.rutaActual,
        queryParams: params,
        replaceUrl: true,
        queryParamsHandling: 'merge',

      });

  }

  applyFilter(event: any) {
    if (this.tiempoFueraId) {
      clearTimeout(this.tiempoFueraId)
    }
    this.tiempoFueraId = setTimeout(() => {
      this.tablaIndicePagina = 0;
      let params: Params = {
        buscar: event,
        pageIndex: this.tablaIndicePagina,
      }
      this.changeParams(params);

      this.getUsuariosPaginados();
    }, 500);
  }

  private getUsuarios(listaUsuarios: User[]) {
    this.users = listaUsuarios;
  }

  private eliminarUsuarioMensaje() {
    this.toast.showSuccessMessage("Usuario eliminado con exito.");
    this.getUsuariosPaginados();
  }

  private mostrarError(error: string) {
    this.toast.showErrorMessage(error);
  }
}


