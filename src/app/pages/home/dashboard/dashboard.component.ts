import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AppApiService } from 'src/app/repository/app-api/app-api.service';
import { ToastService } from 'src/app/services/toast/toast.service';
import { Product } from '../../../classes/Product';
import { ProductLog } from '../../../classes/ProductLog';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  private timeOut: any;

  products: Product[] = [];
  inputLogs: ProductLog[] = [];
  outputLogs: Product[] = [];

  displayedColumns: string[] = ['index', 'name', 'provider', 'reason', 'amount', 'price', 'stock'];
  displayedColumns2: string[] = ['index', 'name', 'provider', 'reason', 'amount', 'price', 'stock'];
  displayedColumns3: string[] = ['index', 'name', 'provider', 'expiryDate', 'price', 'stock'];

  tableLength = 0;
  tableIndex = 0;
  pageLength = 10;

  tableLength2 = 0;
  tableIndex2 = 0;
  pageLength2 = 10;

  tableLength3 = 0;
  tableIndex3 = 0;
  pageLength3 = 10;

  search = "";
  search2 = "";
  search3 = "";

  constructor(private http: HttpClient, private route: Router, private dialog: MatDialog, private toast: ToastService, private activedRoute: ActivatedRoute, private api: AppApiService) { }

  ngOnInit(): void {
    this.getProductsLogs();
  }

  getProductsLogs() {
    ProductLog.getAllInputPag(this.http,
      this.tableIndex,
      this.pageLength,
      this.search,
      (inputLogs) => this.setProductInputLogs(inputLogs),
      (error) => this.showError(error));

    ProductLog.getAllOutputPag(this.http,
      this.tableIndex2,
      this.pageLength2,
      this.search2,
      (outputLogs) => this.setProductOutputLogs(outputLogs),
      (error) => this.showError(error));

    Product.getAllLowStockPag(this.http,
      this.tableIndex3,
      this.pageLength3,
      this.search3,
      (products) => this.setProductLowStock(products),
      (error) => this.showError(error));

    this.toast.showSuccessMessage("Reportes actualizados.");
  }

  setProductInputLogs(logs: ProductLog[]) {
    this.inputLogs = logs;
  }

  setProductOutputLogs(logs: ProductLog[]) {
    this.outputLogs = logs;
  }

  setProductLowStock(products: Product[]) {
    this.products = products;
  }

  private showError(error: string) {
    this.toast.showErrorMessage(error);
  }

  onPageChange(event: PageEvent) {
    this.tableIndex = event.pageIndex;
    this.pageLength = event.pageSize;
    this.getProductsLogs();
  }

  onPageChange2(event: PageEvent) {
    this.tableIndex = event.pageIndex;
    this.pageLength = event.pageSize;
    this.getProductsLogs();
  }

  onPageChange3(event: PageEvent) {
    this.tableIndex = event.pageIndex;
    this.pageLength = event.pageSize;
    this.getProductsLogs();
  }

}
