import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AppApiService } from 'src/app/repository/app-api/app-api.service';
import { ToastService } from 'src/app/services/toast/toast.service';
import { Reason } from '../../../classes/Reason';
import { ActionDialogComponent } from '../../../components/action-dialog/action-dialog.component';

@Component({
  selector: 'app-reasons',
  templateUrl: './reasons.component.html',
  styleUrls: ['./reasons.component.scss']
})
export class ReasonsComponent implements OnInit {

  private timeOut: any;

  reasons: Reason[] = [];

  displayedColumns: string[] = ['index', 'reason', 'action'];

  tableLength = 0;
  tableIndex = 0;
  pageLength = 10;
  search = "";

  dialogoRef?: MatDialogRef<ActionDialogComponent> | null = null;

  constructor(private http: HttpClient, private ruta: Router, private dialogo: MatDialog, private toast: ToastService, private rutaActual: ActivatedRoute, private api: AppApiService) {
    this.rutaActual.queryParams.subscribe(params => {
      this.pageLength = (params['pageLength'] != undefined) ? parseInt(params['pageLength']) : this.pageLength;
      this.tableIndex = (params['pageIndex'] != undefined) ? parseInt(params['pageIndex']) : this.tableIndex;
      this.search = (params['buscar'] != undefined) ? (params['buscar']) : this.search;
    }).unsubscribe();

  }

  ngOnInit(): void {
    this.getReasons();
  }

  onPageChange(event: PageEvent) {
    this.tableIndex = event.pageIndex;
    this.pageLength = event.pageSize;
    let params: Params = {
      pageLength: event.pageSize,
      pageIndex: event.pageIndex,
    }
    this.getReasons();

    this.changeParams(params);
  }

  reasonEdit(id: any) {
    this.ruta.navigate(['razones/edicion/' + id]);
  }

  getReasons() {
    Reason.getAllOutPag(this.http,
      this.tableIndex,
      this.pageLength,
      this.search,
      (reasons) => this.setReasonList(reasons),
      (error) => this.showError(error));
  }

  deleteDialog(id: bigint, description: string) {
    this.dialogoRef = this.dialogo.open(ActionDialogComponent, {
      data: {
        title: 'Advertencia',
        message: `¿Estás seguro de eliminar la razon ${description}?`,
        positiveButton: 'Eliminar', //Si dan click al boton positivo retorna true
        negativeButton: 'Cancelar' //Si dan click al boton Negatibo retorna false
      },
      width: '50%',
      minWidth: '280px',
      maxWidth: '560px'
    });
    this.dialogoRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.reasonDelete(id);
      }
    });
  }

  reasonDelete(id: any) {
    Reason.delete(this.http, id,
      () => this.reasonDeleteMessage(),
      (error) => this.showError(error));
  }

  changeParams(params: Params) {
    this.ruta.navigate([],
      {
        relativeTo: this.rutaActual,
        queryParams: params,
        replaceUrl: true,
        queryParamsHandling: 'merge',

      });

  }

  applyFilter(event: any) {
    if (this.timeOut) {
      clearTimeout(this.timeOut)
    }
    this.timeOut = setTimeout(() => {
      this.tableIndex = 0;
      let params: Params = {
        buscar: event,
        pageIndex: this.tableIndex,
      }
      this.changeParams(params);

      this.getReasons();
    }, 500);
  }

  private setReasonList(reasonsList: Reason[]) {
    this.reasons = reasonsList;
  }

  private reasonDeleteMessage() {
    this.toast.showSuccessMessage("Razon eliminada con exito.");
    this.getReasons();
  }

  private showError(error: string) {
    this.toast.showErrorMessage(error);
  }
}


