import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReasonsEditComponent } from './reasons-edit.component';

describe('ReasonsEditComponent', () => {
  let component: ReasonsEditComponent;
  let fixture: ComponentFixture<ReasonsEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReasonsEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReasonsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
