import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionLog } from '../../../../classes/ActionLog';
import { Reason } from '../../../../classes/Reason';
import { AppApiService } from '../../../../repository/app-api/app-api.service';
import { ToastService } from '../../../../services/toast/toast.service';


@Component({
  selector: 'app-reasons-edit',
  templateUrl: './reasons-edit.component.html',
  styleUrls: ['./reasons-edit.component.scss']
})
export class ReasonsEditComponent implements OnInit {

  form: any;
  errorMessage = null;
  reasonId = null;
  action: string = "";

  constructor(private http: HttpClient, private router: Router, private currentRoute: ActivatedRoute, public toast: ToastService, private formBuider: FormBuilder, private api: AppApiService) {
    this.currentRoute.params.subscribe(res => this.reasonId = res['id']);
  }

  ngOnInit() {
    this.form = this.formBuider.group({
      description: ['', [Validators.required]],
    });

    if (this.reasonId == 0) {
      this.action = "Crear";
    }
    else {
      this.action = "Editar";

      Reason.get(this.http, this.reasonId, (usuario) => this.getReason(usuario), (error) => this.showError(error));
    }
  }

  closeAlert(event: Event) {
    this.errorMessage = null;
  }

  createOrUpdate() {

    let obj = null;
    if (this.form)
      obj = this.form.value;
    this.errorMessage = null;

    var body: Reason = {
      description: obj.description,
      actionLogId: "2",
    }

    if (this.reasonId == 0) {
      Reason.post(this.http, body, (reason) => this.create(reason), (error) => this.showError(error))
    }
    else {
      body.id = this.reasonId;
      Reason.update(this.http, body, (reason) => this.update(reason), (error) => this.showError(error))
    }
  }

  getReason(reason: Reason) {
    this.form?.controls['description'].setValue(reason.description);
  }

  create(reason: Reason) {
    this.toast.showSuccessMessage("Razon creada con exito");
    this.return();
  }

  update(reason: Reason) {
    this.toast.showSuccessMessage("Razos actualizada con exito.");
    this.return();
  }

  showError(error: string) {
    this.toast.showErrorMessage(error);
  }

  return() {
    this.router.navigate(['../razones']);
  }
}
