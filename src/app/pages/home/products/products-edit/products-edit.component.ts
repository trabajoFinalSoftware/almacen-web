import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../../../../classes/Product';
import { ProductLog } from '../../../../classes/ProductLog';
import { Provider } from '../../../../classes/Provider';
import { User } from '../../../../classes/User';
import { AppApiService } from '../../../../repository/app-api/app-api.service';
import { ToastService } from '../../../../services/toast/toast.service';


@Component({
  selector: 'app-products-edit',
  templateUrl: './products-edit.component.html',
  styleUrls: ['./products-edit.component.scss']
})
export class ProductsEditComponent implements OnInit {

  form: any;
  errorMessage = null;
  productId = null;
  providers: Provider[] = [];
  user: User = new User;
  action: string = "";

  constructor(private http: HttpClient, private router: Router, private currentRoute: ActivatedRoute, public toast: ToastService, private formBuider: FormBuilder, private api: AppApiService) {
    this.currentRoute.params.subscribe(res => this.productId = res['id']);
  }

  ngOnInit() {
    this.form = this.formBuider.group({
      name: ['', [Validators.required]],
      expiryDate: ['', [Validators.required]],
      price: ['', [Validators.required]],
      stock: ['', [Validators.required]],
      providerId: ['', [Validators.required]],
    });

    Provider.getAllPag(this.http, 0, 100, "", (providers) => this.getProviders(providers), (error) => this.showError(error));

    if (this.productId == 0) {
      this.action = "Crear";
    }
    else {
      this.action = "Editar";

      Product.get(this.http, this.productId, (product) => this.getProduct(product), (error) => this.showError(error));
    }
  }

  closeAlert(event: Event) {
    this.errorMessage = null;
  }

  createOrUpdate() {

    let obj = null;
    if (this.form)
      obj = this.form.value;
    this.errorMessage = null;

    var body: Product = {
      name: obj.name,
      expiryDate: obj.expiryDate,
      price: obj.price,
      stock: obj.stock,
      providerId: obj.providerId,
    }

    if (this.productId == 0) {

      this.user = JSON.parse(localStorage.getItem("USER") as string);

      var productLogFormat: any = {
        product: body,
        userId: this.user.id,
      }

      Product.postLog(this.http, productLogFormat, (provider) => this.create(provider), (error) => this.showError(error))
    }
    else {
      body.id = this.productId;
      Product.update(this.http, body, (provider) => this.update(provider), (error) => this.showError(error))
    }
  }

  getProviders(providers: Provider[]) {
    this.providers = providers;
  }

  getProduct(product: Product) {
    this.form?.controls['name'].setValue(product.name);
    this.form?.controls['expiryDate'].setValue(product.expiryDate);
    this.form?.controls['price'].setValue(product.price);
    this.form?.controls['stock'].setValue(product.stock);
    this.form?.controls['providerId'].setValue(product.providerId);
  }

  create(provider: Provider) {
    this.toast.showSuccessMessage("Producto registrado con exito");
    this.return();
  }

  update(reason: Provider) {
    this.toast.showSuccessMessage("Producto actualizado con exito.");
    this.return();
  }

  showError(error: string) {
    this.toast.showErrorMessage(error);
  }

  return() {
    this.router.navigate(['../productos']);
  }
}
