import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { AppApiService } from 'src/app/repository/app-api/app-api.service';
import { ToastService } from 'src/app/services/toast/toast.service';
import { Product } from '../../../classes/Product';
import { ActionDialogComponent } from '../../../components/action-dialog/action-dialog.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  private timeOut: any;

  products: Product[] = [];

  displayedColumns: string[] = ['index', 'name', 'provider', 'expiryDate', 'stock', 'price', 'action'];

  tableLength = 0;
  tableIndex = 0;
  pageLength = 10;
  search = "";

  dialogoRef?: MatDialogRef<ActionDialogComponent> | null = null;

  constructor(private http: HttpClient, private route: Router, private dialog: MatDialog, private toast: ToastService, private activedRoute: ActivatedRoute, private api: AppApiService) {
    this.activedRoute.queryParams.subscribe(params => {
      this.pageLength = (params['pageLength'] != undefined) ? parseInt(params['pageLength']) : this.pageLength;
      this.tableIndex = (params['pageIndex'] != undefined) ? parseInt(params['pageIndex']) : this.tableIndex;
      this.search = (params['buscar'] != undefined) ? (params['buscar']) : this.search;
    }).unsubscribe();

  }

  ngOnInit(): void {
    this.getProducts();
  }

  onPageChange(event: PageEvent) {
    this.tableIndex = event.pageIndex;
    this.pageLength = event.pageSize;
    let params: Params = {
      pageLength: event.pageSize,
      pageIndex: event.pageIndex,
    }
    this.getProducts();

    this.changeParams(params);
  }

  edit(id: any) {
    this.route.navigate(['productos/edicion/' + id]);
  }

  getProducts() {
    Product.getAllPag(this.http,
      this.tableIndex,
      this.pageLength,
      this.search,
      (products) => this.setProductList(products),
      (error) => this.showError(error));
  }

  deleteDialog(id: bigint, description: string) {
    this.dialogoRef = this.dialog.open(ActionDialogComponent, {
      data: {
        title: 'Advertencia',
        message: `¿Estás seguro de eliminar el producto ${description}?`,
        positiveButton: 'Eliminar', //Si dan click al boton positivo retorna true
        negativeButton: 'Cancelar' //Si dan click al boton Negatibo retorna false
      },
      width: '50%',
      minWidth: '280px',
      maxWidth: '560px'
    });
    this.dialogoRef.afterClosed().subscribe(result => {
      if (result === true) {
        this.productDelete(id);
      }
    });
  }

  productDelete(id: any) {
    Product.delete(this.http, id,
      () => this.deleteMessage(),
      (error) => this.showError(error));
  }

  changeParams(params: Params) {
    this.route.navigate([],
      {
        relativeTo: this.activedRoute,
        queryParams: params,
        replaceUrl: true,
        queryParamsHandling: 'merge',

      });

  }

  applyFilter(event: any) {
    if (this.timeOut) {
      clearTimeout(this.timeOut)
    }
    this.timeOut = setTimeout(() => {
      this.tableIndex = 0;
      let params: Params = {
        buscar: event,
        pageIndex: this.tableIndex,
      }
      this.changeParams(params);

      this.getProducts();
    }, 500);
  }

  private setProductList(productsList: Product[]) {
    this.products = productsList;
    this.toast.showSuccessMessage("Lista de productos actualizada.");
  }

  private deleteMessage() {
    this.toast.showSuccessMessage("Producto eliminado con exito.");
    this.getProducts();
  }

  private showError(error: string) {
    this.toast.showErrorMessage(error);
  }
}

