import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Login } from '../../classes/Login';
import { User } from '../../classes/User';
import { UserAuth } from '../../classes/UserAuth';
import { AppApiService } from '../../repository/app-api/app-api.service';
import { ToastService } from '../../services/toast/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form: any;

  errorMessage: any = null;

  constructor(private http: HttpClient, private router: Router, private currentRoute: ActivatedRoute, public toast: ToastService, private formBuider: FormBuilder, private api: AppApiService) {
  }

  ngOnInit(): void {
    this.form = this.formBuider.group({
      user: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });

    if (localStorage.getItem("USER") != null) {
      this.router.navigate(['']);
    }

  }

  login() {

    let obj = null;
    if (this.form)
      obj = this.form.value;

    this.errorMessage = null;

    let user: User = {
      nick: obj.user,
      password: obj.password,
    }

    Login.login(
      this.http,
      user,
      (user) => this.loginSuccess(user),
      () => this.showError(),
    );
  }

  getErrorMessage(field: string): string {
    if (field === 'username') {
      return this.form.get(field).hasError('required') ? 'Ingresa un nombre de usuario válido' : '';
    } else if (field === 'password') {
      return this.form.get(field).hasError('required') ? 'Ingresa una contraseña válida' : '';
    }
    return '';
  }

  closeAlert(event: any) {
    this.errorMessage = null;
  }

  loginSuccess(user: UserAuth) {
    localStorage.setItem("TOKEN", user.token);
    localStorage.setItem("USER", JSON.stringify(user.usuario));
    this.router.navigate(['']);
  }

  showError() {
    this.toast.showErrorMessage("Usuario o contrasena incorrecta");
  }


}
