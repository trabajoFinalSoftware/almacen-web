import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

export type AppHttpErrorCallback = (error: string) => void;


@Injectable({
  providedIn: 'root'
})
export class AppApiService {

  static URL = environment.serverURL;

  static LOGIN = 'api/login';

  static USER = 'api/users/';
  static USERS = 'api/users/all';

  static PRODUCT = 'api/products/';
  static PRODUCTS = 'api/products/all';

  static REASON = 'api/reasons/';
  static REASONS = 'api/reasons/all';

  static PROVIDER = 'api/providers/';
  static PROVIDERS = 'api/providers/all';

  static ACTIONLOG = 'api/actionlogs/';
  static ACTIONLOGS = 'api/actionlogs/all';

  static ORDER = 'api/orders/';
  static ORDERS = 'api/orders/all';

  static PAYTYPE = 'api/paytypes/';
  static PAYTYPES = 'api/paytypes/all';

  static PRODUCTLOG = 'api/productlogs/';
  static PRODUCTLOGS = 'api/productlogs/all';

  static PRODUCTORDER = 'api/productorders/';
  static PRODUCTORDERS = 'api/productorders/all';

  static ROLE = 'api/roles/';
  static ROLES = 'api/roles/all';

  constructor(private http: HttpClient) { }

  static getHeaders() {

    let token: string = "";

    if (localStorage.getItem("TOKEN"))
      token = localStorage.getItem("TOKEN") as string;

    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    });
  }

  static get(http: HttpClient, url: string, funcionExito: Function, funcionError: Function) {
    return http.get<any>(AppApiService.URL + url, { headers: AppApiService.getHeaders() }).subscribe(res => {
      if (res.status === 'ok') {
        if (res.result != undefined)
          funcionExito(res.result);
        if (res.results != undefined)
          funcionExito(res.results);
      }
      else {
        console.error(res);
        funcionError(res.error);
      }
    }, error => {
      if (error.status == 401) {
        this.logout()
      }
      console.error(error);
      funcionError(error.getMessage());
    });
  }

  static create(http: HttpClient, url: string, objeto: any, funcionExito: Function, funcionError: Function) {
    return http.post<any>(url, objeto, { headers: AppApiService.getHeaders() }).subscribe(res => {
      if (res.status === 'ok')
        funcionExito(res.result);
      else {
        console.error(res);
        funcionError(res.error);
      }
    }, error => {
      if (error.status == 401) {
        this.logout()
      }
      console.error(error);
      funcionError(error.getMessage());
    });
  }

  static update(http: HttpClient, url: string, objeto: any, funcionExito: Function, funcionError: Function) {
    return http.put<any>(url, objeto, { headers: AppApiService.getHeaders() }).subscribe(res => {
      if (res.status === 'ok')
        funcionExito(res.result);
      else {
        console.error(res);
        funcionError(res.error);
      }
    }, error => {
      if (error.status == 401) {
        this.logout()
      }
      console.error(error);
      funcionError(error.getMessage());
    });
  }

  static deleteLogic(http: HttpClient, url: string, funcionExito: Function, funcionError: Function) {
    return http.put<any>(url, { headers: AppApiService.getHeaders() }).subscribe(res => {
      if (res.status === 'ok')
        funcionExito(res.result);
      else {
        console.error(res);
        funcionError(res.error);
      }
    }, error => {
      if (error.status == 401) {
        this.logout()
      }
      console.error(error);
      funcionError(error.getMessage());
    });
  }

  static logout() {

  }
}
