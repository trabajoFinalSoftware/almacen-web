import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoResultsTableComponent } from './no-results-table.component';

describe('NoResultsTableComponent', () => {
  let component: NoResultsTableComponent;
  let fixture: ComponentFixture<NoResultsTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoResultsTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoResultsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
