import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-no-results-table',
  templateUrl: './no-results-table.component.html',
  styleUrls: ['./no-results-table.component.scss']
})
export class NoResultsTableComponent implements OnInit {

  @Input() noRecords: boolean | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
