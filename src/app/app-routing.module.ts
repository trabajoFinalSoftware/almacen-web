import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './pages/home/dashboard/dashboard.component';
import { HomeComponent } from './pages/home/home.component';
import { ProductsEditComponent } from './pages/home/products/products-edit/products-edit.component';
import { ProductsComponent } from './pages/home/products/products.component';
import { ProvidersEditComponent } from './pages/home/providers/providers-edit/providers-edit.component';
import { ProvidersComponent } from './pages/home/providers/providers.component';
import { ReasonsEditComponent } from './pages/home/reasons/reasons-edit/reasons-edit.component';
import { ReasonsComponent } from './pages/home/reasons/reasons.component';
import { SalePageComponent } from './pages/home/sale-page/sale-page.component';
import { UsersEditComponent } from './pages/home/users/users-edit/users-edit.component';
import { UsersComponent } from './pages/home/users/users.component';
import { LoginComponent } from './pages/login/login.component';
import { NoFoundComponent } from './pages/no-found/no-found.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        component: DashboardComponent,
      },
      {
        path: 'productos',
        component: ProductsComponent,
      },
      {
        path: 'productos/edicion/:id',
        component: ProductsEditComponent,
      },
      {
        path: 'proveedores',
        component: ProvidersComponent,
      },
      {
        path: 'proveedores/edicion/:id',
        component: ProvidersEditComponent,
      },
      {
        path: 'razones',
        component: ReasonsComponent,
      },
      {
        path: 'razones/edicion/:id',
        component: ReasonsEditComponent,
      },
      {
        path: 'ventas',
        component: SalePageComponent,
      },
      {
        path: 'usuarios',
        component: UsersComponent,
      },
      {
        path: 'usuarios/edicion/:id',
        component: UsersEditComponent,
      },
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '404',
    component: NoFoundComponent
  },
  {
    path: '**',
    redirectTo: '404'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
