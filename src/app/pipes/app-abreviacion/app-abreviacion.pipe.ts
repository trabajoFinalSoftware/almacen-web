import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appAbreviacion'
})
export class AppAbreviacionPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    if ((value !== null) && (value !== undefined)) {
      if (value.hasOwnProperty('firstName') && value.hasOwnProperty('lastName')) {
        let firstName: string = value.nombres;
        let lastName: string = value.apellidos;
        return `${firstName.substring(0, 1).toUpperCase()}${lastName.substring(0, 1).toUpperCase()}`;
      }
    }
    return '';  
  }

}
