import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';


@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private snackar: MatSnackBar) { }

  private setMessages(message: string, action: string, clase: string, durationSeconds: number, positionHorizontal: MatSnackBarHorizontalPosition) {
    this.snackar.open(message, action, {
      panelClass: [clase],
      duration: durationSeconds * 1000,
      horizontalPosition: positionHorizontal
    });
  }

  public showError(error: Error, duration: number = 5) {
    let mensaje = "";
    switch (error.name) {
      case 'HttpErrorResponse':
        mensaje = "Error de conexión con el servidor"
        break;
      default:
        mensaje = error.message;
        break;
    }
    this.showErrorMessage(mensaje, duration);
  }

  public showErrorMessage(message: string, duration: number = 5) {
    this.setMessages(message, '✗', 'error-snackbar', duration, 'center');
  }

  public showWarningMessage(error: string, duration: number = 5) {
    this.setMessages(error, '!', 'warning-snackbar', duration, 'center');
  }

  public showSuccessMessage(message: string, duration: number = 3) {
    this.setMessages(message, '✓', 'success-snackbar', duration, 'center');
  }

}
